from django.test import LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException

from feed.models import Link

from feed import utils
from feed.constants import tests_url_helpers as helper, strings

import time, os

MAX_WAIT = 2

class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

        staging_server = os.environ.get('STAGING_SERVER')
        if staging_server:
            self.live_server_url = 'http://' + staging_server

        Link.objects.create(text=helper.EXAMPLE_LINK.replace('.com', '.org'), status='W')
        Link.objects.create(text=helper.EXAMPLE_LINK.replace('.com', '.net'), status='W')

    def tearDown(self):
        self.browser.quit()

    def get_link_input_box(self):
        link_input = self.browser.find_element_by_id('id_text')
        link_input.clear()
        return link_input

    def set_valid_captcha(self):
        captcha_input = self.browser.find_element_by_id('id_captcha_1')
        captcha_input.clear()
        captcha_input.send_keys("PASSED")

    def set_invalid_captcha(self, empty=False):
        captcha_input = self.browser.find_element_by_id('id_captcha_1')
        captcha_input.clear()
        if empty:
            return
        captcha_input.send_keys("blahblahblah")

    def wait_for(self, fn):
        start_time = time.time()
        while True:
            try:
                return fn()
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def test_user_can_send_a_link_for_confirmation(self):
        # hasan has heard about a cool new online blog collector. he goes
        # to check out its homepage
        self.browser.get(self.live_server_url)
        # he notices header and page title mention Panjere
        assert strings.SITE_TITLE in self.browser.title
        header_text = self.browser.find_element_by_tag_name('h3').text
        self.assertIn(strings.SITE_TITLE, header_text)

        # he invited to enter a blog link
        inputbox = self.get_link_input_box()
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            strings.FORM_INPUT_PALCE_HOLDER_TEXT
        )

        # he types "http://example.com"
        inputbox.send_keys(helper.EXAMPLE_LINK)
        self.set_valid_captcha()

        # when he hits enter the page updates
        # and the page shows a message saying the link
        # was sent for admin's approval
        inputbox.send_keys(Keys.ENTER)
        time.sleep(0.5)

        self.wait_for(lambda: self.assertIn(
            strings.FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE,
            self.browser.page_source
        ))

        # hasan loads the home page and sees some links
        # links are labled with their title
        table = self.browser.find_element_by_id('id_link_table')
        link = table.find_element_by_tag_name('a')
        (link_label, link_url) = (link.text, link.get_attribute('href'))
        self.assertEqual(utils.get_web_page_title(link_url), link_label)

        # hasan accidently tries to submit an empty link
        # after page updte he sees an error message
        inputbox = self.get_link_input_box()
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.browser.find_elements_by_css_selector(
            '#id_text:invalid'
        ))

        # page updates
        # hasan tries to submit an already submited link
        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_LINK)
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            strings.FORM_LINK_ALREADY_EXISTS_ERROR
        ))

        # hasan submits an invalid url
        # page shows invalid url error
        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_INVALID_LINK)
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            strings.FORM_INVALID_URL_ERROR,
        ))

        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_UAVAILABLE_LINK)
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR,
        ))

        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_NON_EXISTENT_LINK)
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR,
        ))

        # he tries to submit another link but enters an invalid captcha
        # page updates and hasan sees a captcha invalid error message
        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_LINK.replace("com", "net"))
        self.set_invalid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has-captcha-error').text,
            strings.FORM_INVALID_CAPTCHA_ERROR
        ))

        # hasan accidently enters empty captcha
        # after page updte he sees an error message
        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_LINK.replace("com", "net"))
        self.set_invalid_captcha(empty=True)

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.browser.find_elements_by_css_selector(
            '#id_captcha_1:invalid'
        ))

        inputbox = self.get_link_input_box()
        inputbox.send_keys(helper.EXAMPLE_RESTRICTED_LINK)
        self.set_valid_captcha()

        inputbox.send_keys(Keys.ENTER)
        self.wait_for(lambda: self.assertIn(
            strings.FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE,
            self.browser.page_source
        ))



    def test_layout_and_styling(self):
        # hasan visits the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # he notices the input box is nicely centered
        inputbox = self.get_link_input_box()
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )

        inputbox.send_keys('testing')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(0.5)
        inputbox = self.get_link_input_box()
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )
