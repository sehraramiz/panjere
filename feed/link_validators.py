import requests
from urllib.parse import urlparse
import dns.resolver
from dns.resolver import NXDOMAIN

"""
    status B: Broken links that probably exist but
        we can't reach them (restricted, 403, filtershodegan, ...)
    status U: Unavailable links with unknown hosts or 404 status,
        we can't reach them what so ever
    status W: links that are available and healthy
"""

def get_link_status(link):
    domain = urlparse(link).netloc
    with_no_subdomain = '.'.join(domain.split('.')[-2:])
    status = 'U'
    try:
        result = dns.resolver.resolve(with_no_subdomain, 'A')
        if '10.10' in str(result[0]):
            return 'B'
    except NXDOMAIN:
        # The DNS query name does not exist
        # validation error Unreachable
        return 'U'
    except Exception as e:
        return 'B'

    # no domain problems proceed to other validators
    try:
        response = requests.get(link)
        if response.status_code == 404:
            return 'U'
        elif response.status_code in [200, 301]:
            return 'W'
        else:
            return 'B'
    except Exception as e:
        return 'B'
    return 'U'
