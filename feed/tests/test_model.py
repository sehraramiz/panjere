from django.test import TestCase
from django.core.exceptions import ValidationError

from feed.models import Link

from feed import utils
from feed.constants import tests_url_helpers as helper


class LinkModelTest(TestCase):

    def setUp(self):
        self.EXAMPLE_LINK_TITLE = utils.get_web_page_title(helper.EXAMPLE_LINK)

    def test_default_text(self):
        link = Link()
        self.assertEqual(link.text, '')
        self.assertEqual(link.status, 'P')

    def test_cannot_save_blank_link_text(self):
        link = Link(text="")
        with self.assertRaises(ValidationError):
            link.save()
            link.full_clean()

    def test_duplicate_items_are_invalid(self):
        Link.objects.create(text=helper.EXAMPLE_LINK)
        with self.assertRaises(ValidationError):
            link = Link(text=helper.EXAMPLE_LINK)
            link.full_clean()

    def test_string_representation(self):
        link = Link(text=helper.EXAMPLE_LINK)
        self.assertEqual(str(link), helper.EXAMPLE_LINK)

    def test_list_ordering(self):
        link1 = Link.objects.create(text=helper.EXAMPLE_LINK)
        link2 = Link.objects.create(text=helper.EXAMPLE_LINK.replace('com', 'org'))
        link3 = Link.objects.create(text=helper.EXAMPLE_LINK.replace('com', 'net'))
        self.assertEqual(
            list(Link.objects.all()),
            [link1, link2, link3]
        )

    def test_cannot_save_unavailable_web_page(self):
        link = Link(text=helper.EXAMPLE_UAVAILABLE_LINK)
        with self.assertRaises(ValidationError):
            link.save()
            link.full_clean()

    def test_cannot_save_invalid_link(self):
        link = Link(text=helper.EXAMPLE_INVALID_LINK)
        self.assertEqual(Link.objects.count(), 0)

    def test_cannot_save_non_existent_link(self):
        link = Link(text=helper.EXAMPLE_NON_EXISTENT_LINK)
        with self.assertRaises(ValidationError):
            link.save()
            link.full_clean()

    def test_saves_link_title(self):
        link = Link.objects.create(text=helper.EXAMPLE_LINK)
        self.assertEqual(link.title, self.EXAMPLE_LINK_TITLE)

    def test_strips_ending_slash_in_url(self):
        link = Link.objects.create(text=helper.EXAMPLE_LINK + '/')
        self.assertEqual(link.text, helper.EXAMPLE_LINK)

    def test_submits_restricted_web_pages_with_no_title(self):
        link = Link.objects.create(text=helper.EXAMPLE_RESTRICTED_LINK)
        self.assertEqual(link.title, "no title")
