import requests, json


BOT_TOKEN = '1139217456:AAF9akVfc2D8qqE2TeX7oZPiSEzJW6Bamic'
BASE_URL = f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage"
ADMINS_CHAT_IDS = {
    'amir': '133170939',
    'ali': '48017262',
}
CHANNEL_CHAT_ID = '-1001195206553'

def send_link_to_telegram_channel(publish_data, get_manual_publish_url=None):

    msg = publish_data["title"] + "\n" + publish_data["url"]

    manual_publish_url = BASE_URL + f"?chat_id={CHANNEL_CHAT_ID}&text={msg}"

    if get_manual_publish_url:
        return manual_publish_url

    data = {
        'chat_id': CHANNEL_CHAT_ID,
        'text': msg,
    }

    try:
        requests.post(BASE_URL, data=data)
    except Exception as e:
        raise e

def send_new_link_to_admin(link):

    reply_markup={
        "inline_keyboard": [
            [
                {
                    "text": "Well",
                    "url": "{}setstatus/W/{}".format(
                        link['server_url'],
                        link['id'],
                    )
                }
            ]
        ]
    }
    data = {
        'chat_id': '',
        'text': link['url'],
        'reply_markup': json.dumps(reply_markup)
    }

    for chat_id in ADMINS_CHAT_IDS.values():
        data.update({ 'chat_id': chat_id })
        try:
            requests.post(BASE_URL, data=data)
        except Exception as e:
            raise e

