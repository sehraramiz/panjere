from django.db import models
from django.utils.html import format_html
from django.core.exceptions import ValidationError
from urllib.parse import urlparse

from feed import utils
from feed.telegram import send_link_to_telegram_channel
from feed.constants import strings
from feed.link_validators import get_link_status


class Link(models.Model):
    text = models.URLField(
        unique=True,
        error_messages={
            'blank': strings.FORM_EMPTY_LINK_ERROR,
            'required': strings.FORM_REQUIRED_LINK_ERROR,
        }
    )

    title = models.TextField(default="no title")

    domain = models.TextField(default="no domain")

    # PENDING: links that are waiting for approval
    # WELL: approved links that we show in the feed
    # BROKEN: broken links (404, 403, ...)
    # FISHY: links to sensitive content
    status_choices = (
        ('P', 'Pending'),
        ('W', 'Well'),
        ('B', 'Broken'),
        ('F', 'Fishy'),
    )
    status = models.TextField(
            default='P',
            choices=status_choices
    )

    has_published = models.BooleanField(default=False)

    def clean(self):
        self.clean_fields()
        self.text = self.text.strip("/")

        if self.status != "W":
            link_status = get_link_status(self.text)
            if link_status == "B":
                self.status = "B"
            elif link_status == "U":
                raise ValidationError(
                    { 'text': strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR },
                    code="unavailable",
                )

    def save(self, *args, **kwargs):
        self.full_clean()

        # so if a page is broken we send it
        # for confirmation without getting the title and domain
        if self.title == "no title" and self.status != "B":
            self.title = utils.get_web_page_title(self.text)

            self.domain = urlparse(self.text).netloc
            self.domain = '.'.join(self.domain.split('.')[-2:])

        # self.publish_to_telegram()

        super().save(*args, **kwargs)



    def __str__(self):
        return self.text

    def clickable_link(self):
        return format_html(
            ' <a href="{}">{}</a> ',
            self.text,
            self.title,
        )


    @property
    def manual_publish_url(self):
        publish_data = {
            "url": self.text,
            "title": self.title
        }
        publish_url = send_link_to_telegram_channel(
            publish_data,
            get_manual_publish_url=True
        )

        return publish_url

    def publish_to_telegram(self):
        if self.status == 'W' and not self.has_published:
            try:
                publish_data = {
                    "url": self.text,
                    "title": self.title
                }
                send_link_to_telegram_channel(publish_data)
                self.has_published = True
                self.save(update_fields=["has_published"])
            except Exception as e:
                # TODO raise e
                pass

    class Meta:
        ordering = ('id',)
