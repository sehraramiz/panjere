from django import forms
from captcha.fields import CaptchaField

from feed.models import Link
from feed.constants import strings


class LinkForm(forms.models.ModelForm):
    captcha = CaptchaField(error_messages={
        'required': strings.FORM_EMPTY_CAPTCHA_ERROR,
        'invalid': strings.FORM_INVALID_CAPTCHA_ERROR,
    })

    class Meta:
        model = Link
        fields = ('text', 'captcha')
        widgets = {
            'text': forms.fields.URLInput(attrs={
                'placeholder': strings.FORM_INPUT_PALCE_HOLDER_TEXT,
                'class': 'form-control input-lg',
            }),
        }
        error_messages = {
            'text': {
                'required': strings.FORM_REQUIRED_LINK_ERROR,
                'blank': strings.FORM_EMPTY_LINK_ERROR,
                'invalid': strings.FORM_INVALID_URL_ERROR,
                'unavailable': strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR,
                'unique': strings.FORM_LINK_ALREADY_EXISTS_ERROR,
            },
        }
