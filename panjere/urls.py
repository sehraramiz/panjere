"""panjere URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from feed import views as feed_views
from frame import views as frame_views
from django.contrib import admin

urlpatterns = [
    path('', feed_views.home_page),
    path('submit/', feed_views.submit_form),
    path('about/', TemplateView.as_view(template_name='about.html')),
    re_path(r'api/random/(?P<count>\d*)', frame_views.get_random_links_by_count),
    path('setstatus/<str:status>/<int:id>/',feed_views.set_status),
    path('admin/', admin.site.urls),
    path('captcha/', include('captcha.urls')),
    path('frame/', frame_views.frame_sample),
]
