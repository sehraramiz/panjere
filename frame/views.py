from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse

from feed.models import Link

import random, json, os


from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from frame.serializers import LinkSerializer

def get_random_links_by_count_from_db(count):
    links = Link.objects.filter(status="W")

    if count > len(links) or count == 0:
        random_links = links
    else:
        random_links = random.sample(list(links), count)

    return random_links

@api_view(['GET'])
@renderer_classes([TemplateHTMLRenderer, JSONRenderer])
def get_random_links_by_count(request, count):
    count = int(count)

    links = get_random_links_by_count_from_db(count)

    if request.accepted_renderer.format == 'html':
        data = {'links': links}
        return Response(data, template_name='frame.html')

    serializer = LinkSerializer(links, many=True)
    return Response(serializer.data)

def frame_sample(request):
    current_url = request.build_absolute_uri('/')

    # if requests are on https iframe sample url niiz have to be on https
    is_on_https = os.environ.get('IS_ON_HTTPS', False) == "True"
    if is_on_https:
        current_url = current_url.replace("http:", "https:")

    return render(
        request,
        'panjere_frame.html',
        {'server_url': current_url}
    )
